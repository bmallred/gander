package main

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

func walk(directory string, done chan struct{}) chan string {
	c := make(chan string)
	re := regexp.MustCompile(`(LICEN[SC]E|COPY)`)

	go func() {
		defer close(c)

		filepath.Walk(directory, func(p string, fi os.FileInfo, e error) error {
			if e != nil {
				return e
			}

			if !fi.IsDir() {
				if re.Match([]byte(fi.Name())) {
					c <- p
				}
			}

			return nil
		})
	}()

	return c
}

func statusByFile(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}

func statusByDirectory(directory string) bool {
	path := make(chan string)
	done := make(chan struct{})
	go func(d chan struct{}, ch chan string, dir string) {
		for p := range walk(dir, d) {
			ch <- p
		}
	}(done, path, directory)

	status := false
	working := true
	for working {
		select {
		case p := <-path:
			status = statusByFile(p)
			if status {
				working = false
			}
		case <-done:
			working = false
		case <-time.After(6 * time.Second):
			working = false
			status = false
		}
	}
	return status
}

func pidFile(unit Unit, params ...string) string {
	status := false
	path := params[0]
	if strings.HasSuffix(path, ".pid") {
		status = statusByFile(path)
	} else {
		status = statusByDirectory(path)
	}

	desc := params[1]
	if status {
		desc = params[2]
	}
	return fmt.Sprintf(unit.Format, desc)
}
