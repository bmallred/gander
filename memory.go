package main

import (
	"bufio"
	"fmt"
	"os"
)

func memoryUsage(unit Unit, params ...string) string {
	file, err := os.Open("/proc/meminfo")
	if err != nil {
		return fmt.Sprintf(unit.Format, "-")
	}
	defer file.Close()

	total, used, done := 0, 0, 0
	for info := bufio.NewScanner(file); done != 15 && info.Scan(); {
		prop, val := "", 0
		if _, err = fmt.Sscanf(info.Text(), "%s %d", &prop, &val); err != nil {
			return fmt.Sprintf(unit.Format, "-")
		}

		switch prop {
		case "MemTotal:":
			total = val
			used += val
			done |= 1
		case "MemFree:":
			used -= val
			done |= 2
		case "Buffers:":
			used -= val
			done |= 4
		case "Cached:":
			used -= val
			done |= 8
		}
	}

	return color(unit, used*100/total)
}
