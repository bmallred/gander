package main

import (
	"os/exec"
	"strings"
	"time"
)

const (
	fieldSeparator = " "

	// TODO: Look into using colors for various thresholds
	alertColor   = ""
	warningColor = ""
	normalColor  = ""
)

var (
	units = []Unit{
		Unit{
			Format:     "[vpn: %s]",
			Func:       pidFile,
			Parameters: []string{"/var/run/openvpn.pid", "off", "on"},
		},
		Unit{
			Format:     "[rx: %s tx: %s]",
			Func:       networkUsage,
			Parameters: []string{"enp", "wlp"},
		},
		Unit{
			Format:           "[load: %s%%]",
			AlertThreshold:   70,
			WarningThreshold: 50,
			Func:             processorUsage,
		},
		Unit{
			Format:           "[mem: %s%%]",
			AlertThreshold:   70,
			WarningThreshold: 50,
			Func:             memoryUsage,
		},
		Unit{
			Format: "[Mon 02 15:04:05]",
			Func:   clock,
		},
	}
)

func main() {
	for {
		now := time.Now()
		status := []string{}
		for _, unit := range units {
			status = append(status, unit.Func(unit, unit.Parameters...))
		}
		_ = exec.Command("xsetroot", "-name", strings.Join(status, fieldSeparator)).Run()
		time.Sleep(now.Truncate(time.Second).Add(time.Second).Sub(now))
	}
}
