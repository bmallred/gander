package main

import (
	"fmt"
	"strconv"
)

type Unit struct {
	Format           string
	AlertThreshold   int
	WarningThreshold int
	Func             func(unit Unit, params ...string) string
	Parameters       []string
}

func color(unit Unit, i int) string {
	si := strconv.Itoa(i)
	if i >= unit.AlertThreshold {
		return fmt.Sprintf(unit.Format, alertColor+si+normalColor)
	}
	if i >= unit.WarningThreshold {
		return fmt.Sprintf(unit.Format, warningColor+si+normalColor)
	}
	return fmt.Sprintf(unit.Format, si)
}
