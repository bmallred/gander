package main

import "time"

func clock(unit Unit, params ...string) string {
	return time.Now().Local().Format(unit.Format)
}
