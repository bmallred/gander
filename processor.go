package main

import (
	"fmt"
	"io/ioutil"
	"runtime"
)

var (
	cores = runtime.NumCPU()
)

func processorUsage(unit Unit, params ...string) string {
	load := float32(0.0)
	avg, err := ioutil.ReadFile("/proc/loadavg")
	if err != nil {
		return fmt.Sprintf(unit.Format, "-")
	}
	_, err = fmt.Sscanf(string(avg), "%f", &load)
	if err != nil {
		return fmt.Sprintf(unit.Format, "-")
	}
	return color(unit, int(load*100.0/float32(cores)))
}
