package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strings"
)

var (
	rxOld = 0
	txOld = 0
)

const (
	bps            = "B"
	kbps           = "kB"
	mbps           = "MB"
	gbps           = "GB"
	tbps           = "TB"
	pbps           = "PB"
	ebps           = "EB"
	zbps           = "ZB"
	ybps           = "YB"
	floatSeparator = "."
)

func odometer(count int) int {
	speed := 0
	for count > 1000 {
		count /= 1000
		speed++
	}
	return speed
}

func networkUnits(count int) string {
	if count < 0 {
		return "-"
	}

	speed := odometer(count)
	suffixes := []string{bps, kbps, mbps, gbps, tbps, pbps, ebps, zbps, ybps}
	suffix := suffixes[speed]
	rate := float64(count) / math.Pow(10.0, float64(3.0*speed))
	return strings.Replace(fmt.Sprintf("%6.2f %2s", rate, suffix), ".", floatSeparator, 1)
}

func networkUsage(unit Unit, params ...string) string {
	file, err := os.Open("/proc/net/dev")
	if err != nil {
		return fmt.Sprintf(unit.Format, "-", "-")
	}
	defer file.Close()

	void := 0
	dev, rx, tx, rxNow, txNow := "", 0, 0, 0, 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		_, err = fmt.Sscanf(scanner.Text(), "%s %d %d %d %d %d %d %d %d %d",
			&dev,  //  00. dev->name
			&rx,   //  01. rx_bytes
			&void, //  02. rx_packets
			&void, //  03. rx_errors
			&void, //  04. rx_dropped + rx_missed_errors
			&void, //  05. rx_fifo_errors
			&void, //  06. rx_length_errors + rx_over_errors + rx_crc_errors + rx_frame_errors
			&void, //  07. rx_compressed
			&void, //  08. multicast
			&tx,   //  09. tx_bytes
			&void, //  10. tx_packets
			&void, //  11. tx_errors
			&void, //  12. tx_dropped
			&void, //  13. tx_fifo_errors
			&void, //  14. collistions
			&void, //  15. tx_carrier_errors + tx_aborted_errors + tx_window_errors + tx_heartbeat_errors
			&void, //  16. tx_compressed
		)
		for _, network := range params {
			if strings.HasPrefix(dev, network) {
				rxNow += rx
				txNow += tx
			}
		}
	}

	defer func() { rxOld, txOld = rxNow, txNow }()
	return fmt.Sprintf(unit.Format, networkUnits(rxNow-rxOld), networkUnits(txNow-txOld))
}
